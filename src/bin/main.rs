
use std::fs::File;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::thread;
use std::time::Duration;
use rust_web_server::ThreadPool;

///This is a demo that will cover HTTP requests and responses via TCP sockets.
///Might work on UDP at a later stage just to be clever - but we'll see.
///Fuck TCP


fn main() {
	//Rust on a numpad is 7878
	let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
	let pool = ThreadPool::new(4);
	//Bind is synonymous with the new keyword - where a new TcpListener is instantiated and bound
	//Ergo - binding to a port
	for stream in listener.incoming().take(2) {
		//unwrap to stop the program if there's an error
		let stream = stream.unwrap();
		pool.execute(|| {
			handle_connection(stream);
		});
	}
	println!("shutting down");
}

//Hand coded HTTP Request, Response
fn handle_connection(mut stream: TcpStream) {
	let mut buffer = [0; 512];
	println!("Connection estaboulished");
	//pass the buffer to stream.read
	stream.read(&mut buffer).unwrap();
	//String::from_utf8_lossy returns a <?> icon if it find an invalid UTF8 sequence
	// println!("Request {}", String::from_utf8_lossy(&buffer[..]));
	
	//Hardcoded variables - this transforms the get into a byte string with the
	// b" syntax
	let get = b"GET / HTTP/1.1\r\n";
	let sleep = b"GET /sleep HTTP/1.1\r\n";
	
	let (status_line, filename) = if buffer.starts_with(get) {
		("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
	} else if buffer.starts_with(sleep) {
		thread::sleep(Duration::from_secs(5));
		("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
	} else {
		("HTTP/1.1 404 NOT FOUND\r\n\r\n", "404.html")
	};
	
	let mut file = File::open(filename).unwrap();
	let mut contents = String::new();
	
	file.read_to_string(&mut contents).unwrap();
	let response = format!("{}{}", status_line, contents);
	
	//You'd  normally error handle here, but whatever. Follow da big beige book!
	stream.write(response.as_bytes()).unwrap();
	stream.flush().unwrap();
}

