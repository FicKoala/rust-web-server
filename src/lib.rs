use std::process::id;
use std::sync::{Arc, mpsc, Mutex};
use std::thread;

use crate::Message::NewJob;

pub struct ThreadPool {
	workers: Vec<Worker>,
	//Adjust channel types
	sender: mpsc::Sender<Message>,
}

//TASK - Challenge from Rust Book on Page 695
//complete
type Job = Box<dyn FnBox + Send + 'static>;

impl ThreadPool {
	pub fn new(size: usize) -> ThreadPool {
		assert!(size > 0);
		//with capacity preallocates space in the vector
		let (sender, receiver) = mpsc::channel();
		let receiver = Arc::new(Mutex::new(receiver));
		let mut workers = Vec::with_capacity(size);
		for id in 0..size {
			//store workers in workers vector
			workers.push(Worker::new(id, Arc::clone(&receiver)));
		}
		ThreadPool {
			workers,
			sender,
		}
	}
	//FnOnce() takes no parameters and returns nothing
	//Creating a closure and send the job down the sending channel end
	pub fn execute<F>(&self, f: F) where F: FnOnce() + Send + 'static
	{
		let job = Box::new(f);
		self.sender.send(Message::NewJob(job)).unwrap();
	}
}

//Reasoning for two loops is to prevent a deadlock when one worker is busy working on a process
//and the other is waiting for it finish
// This method helps by sending the same number of terminate messages as there are workers before
// join is called on the thread
impl Drop for ThreadPool {
	fn drop(&mut self) {
		println!("Sending terminate message to all workers");
		for _ in &mut self.workers{
			self.sender.send(Message::Terminate).expect("Terminated");
		}
		println!("Shutting down all workers");
		for worker in &mut self.workers {
			println!("Shutting down worker {}", worker.id);
			//Destructure the Some and get the thread
			if let Some(thread) = worker.thread.take() {
				thread.join().expect("worker has been shut down:");
			}
		}
	}
}

pub struct Worker {
	id: usize,
	thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
	//Thread safe smart pointers to handle everything
	//Allow multiple workers own the receiver all while reducing race conditions
	fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
		let thread = thread::spawn(move || {
			//Use loop instead of while to acquire the mutex lock and a joh within the block.
			// This drops the MuteGuard returned from lock as soon as the let job is completed
			loop {
				let message = receiver.lock().expect("job failed on line: 50").recv().unwrap();
				//Call mutex and panic via unwrap. This is in case the mutex is poisoned
				//recv makes thread wait until theres an available job
				
				match message {
					Message::NewJob(job) => {
						println!("Worker {} got a job: executing", id);
						
						job.call_box();
					}
					Message::Terminate => {
						println!("Worker {} was terminated", id);
						break;
					}
				}
			}
		});
		//Add to the vector
		Worker {
			id,
			thread: Some(thread),
		}
	}
}

trait FnBox {
	fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
	fn call_box(self: Box<F>) {
		//move closure out of Box
		(*self)()
	}
}

enum Message
{
	NewJob(Job),
	Terminate,
}